__author__ = 'gerhardleroux'

from load_doc import doc_loader
from Item import Item
from handlers import Heading
from handlers import Image
from handlers import Table
from handlers import par
from handlers import Req_tableform

class ItemTest(Item):

    def __init__(self):
        #set handlers
        self.handlers_map = {}#maps a type to a handling object
        self.handlers_map['Requirement Heading'] = Heading()
        self.handlers_map['TM Req'] = "TBD"
        self.handlers_map['Requirement Image'] = Image()
        #self.handlers_map['Allocation Table'] = AllocT()
        self.handlers_map['Requirement Table'] = Table()
        self.handlers_map['Requirement Paragraph'] = par()
        self.req_table_handler = Req_tableform()

    def populate_custom(self,item):
        self.ID = item['Id']
        self.NAME = item['Name']
        self.TEXT = item['Text']
        self.SOURCE = item['Source']
        self.PARENT = item['Parent']
        self.VERIFY = None#item['Verify Method']
        self.TYPE = item['Type']
        self.ORDER = None#item['Order']
        self.IMAGE_SIZE = "6" #item['Image Size']
        self.ASSUMPTION = None#item['Assumption']

if __name__ == '__main__':
    Item = ItemTest()
    doc = doc_loader(Item,'template.docx')
    doc.load_data('test matrix.csv')
    doc.save('test.docx')

