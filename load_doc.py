__author__ = 'gerhardleroux'
__doc__ = 'this module writes dictionary list data into a word doc based on a predefined interpreted structure '

from docx import Document

import csv
from Item import Item




class doc_loader:


    def __init__(self,item,filename = 'Requirements template.docx'):
        #self.data_Item = Item()
        self.data_Item = item
        self._document = Document(filename)
        self.set_req_as_tables = True

    def _openData(self,filename):#loads a list of requirements sorted based on the value of 'Order' and appends a nestlevel to the data

        with open(filename, 'rU') as csvfile:
            reader = csv.DictReader(csvfile)
            data = []
            for row in reader:
                data.append(row)
        if 'Order' in data:#if the client wanted his data to be sorted based on a "Order" parameter
            data.sort(key=lambda k: float(k['Order']))
        return data

    def load_data(self,filename): #load data set into doc

        #loads csv file into a data matrix variable
        data = self._openData(filename)

        start_reqtable = True
        reqtable = None
        for row in data:
            self.data_Item.populate(row)
            if self.set_req_as_tables:
                if self.data_Item.IS_REQUIREMENT_HEADING:
                    start_reqtable = True
                if self.data_Item.IS_REQUIREMENT_TM:
                    handler = self.data_Item.req_table_handler
                    if start_reqtable:
                        reqtable = handler.loadtable(self._document)
                        start_reqtable = None
                    handler.affect_table(reqtable,self.data_Item)
                else:
                    handler = self.data_Item.handler
                    handler.affect_document(self._document,self.data_Item)
            else:
                handler = self.data_Item.handler
                handler.affect_document(self._document,self.data_Item)

    def save(self,filename = 'Requirements.docx'):
        self._document.save(filename)

