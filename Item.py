__author__ = 'gerhardleroux'
__doc__ = '#maps and encapsulates a row exported from CAMEO to a pre-formated text structure'
import re



class Item:
    def __init__(self,item):
        self.handlers_map = None
        self.TYPE = None

    def populate_custom(self,item):
        pass #default does noting

    def populate(self,item):
        self.populate_custom(item)#delegates populating to customised definition
        self.handler = self.handlers_map[self.TYPE]
        self._clean()
        self._get_logical_state()

    def _clean(self):

        if self.TEXT:
            self.TEXT = self.TEXT.replace('\xe2\x80\x99','\'')#known funnys
        self.TEXT = unicode(self.TEXT,errors = 'replace').replace(u"\ufffd", " ")
        self.NAME = unicode(self.NAME,errors = 'replace').replace(u"\ufffd", " ")
        if self.SOURCE:
            self.SOURCE = unicode(self.SOURCE,errors = 'replace').replace(u"\ufffd", " ")
        if self.VERIFY:
            self.VERIFY = unicode(self.VERIFY,errors = 'replace').replace(u"\ufffd", " ")
        if self.ASSUMPTION:
            self.ASSUMPTION = self.ASSUMPTION.replace('\xe2\x80\x99','\'')#known funnys
            self.ASSUMPTION = unicode(self.ASSUMPTION,errors = 'replace').replace(u"\ufffd", " ")

    def _get_logical_state(self):
        r = re.compile('TM.A.')
        self.IS_ASSUMPTION = None
        if self.ASSUMPTION:
            if r.search(self.ASSUMPTION):#means there is a assumption tracing to this requirement
                if r.search(self.TEXT): #makes sure the assumption is not already inside the text
                 self.IS_ASSUMPTION = None
                else:
                    self.IS_ASSUMPTION = True
        if self.NAME =="":
            self.IS_NAME_EMPTY = True
        elif self.NAME == self.ID:
            self.IS_NAME_EMPTY = True
        else:
            self.IS_NAME_EMPTY = None
        if self.TYPE == 'Requirement Heading':
           self.IS_REQUIREMENT_HEADING = True
        else:
            self.IS_REQUIREMENT_HEADING = None

        if self.TYPE ==  'TM Req':
            self.IS_REQUIREMENT_TM = True
        else:
            self.IS_REQUIREMENT_TM = None

        self.NESTLEVEL = self._getnest()


    def _getnest(self): #get the number of "digit" instances in a string so as to determine the nesting level
        if self.IS_REQUIREMENT_HEADING:
            str = self.ID
        else:
            str = self.PARENT
        r = re.compile('\d+')
        l = r.findall(str)
        return l.__len__()
