# README #

This is just a sharing space for engineers working on the SKA project to swap useful scripts and other code snippets. 


### Who do I talk to? ###

If this sounds like it would be useful - please speak to:

* Mark Nicol <mark.nicol@stfc.ac.uk>
* Gerhard Le Roux <gerhard@ska.ac.za>