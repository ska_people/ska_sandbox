__author__ = 'gerhardleroux'
__doc__ = 'family of objects that present an item on a word doc based on a standard interface'
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
import re
import csv

from docx.shared import Inches
class caption:

    def __init__(self,text,nr):
        self.text = text
        self.nr = nr

    def addCaptionF(self,document):
        paragraph = document.add_paragraph()
        paragraph.add_run('Figure ')
        p = paragraph._p  # this is the actual lxml element for a paragraph
        fld_xml = '<w:fldSimple '+nsdecls('w')+' w:instr=" SEQ Figure\\r'+self.nr+' \* ARABIC "/>'
        fldSimple = parse_xml(fld_xml)
        p.append(fldSimple)
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        paragraph.add_run(': '+ self.text)

    def addCaptionT(self,document):
        paragraph = document.add_paragraph()
        paragraph.add_run('Table ')
        p = paragraph._p  # this is the actual lxml element for a paragraph
        fld_xml = '<w:fldSimple '+nsdecls('w')+' w:instr=" SEQ Table\\r'+self.nr+'  \* ARABIC "/>'
        fldSimple = parse_xml(fld_xml)
        p.append(fldSimple)
        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
        paragraph.add_run(': '+ self.text)

class handle_doc:
    def affect_document(self,document,item):
        pass

class Heading(handle_doc):
    def affect_document(self,document,item):
        r = re.compile('\d+')
        l = r.findall(item.ID)
        nestlevel = l.__len__()
        document.add_heading(item.NAME,nestlevel)

class  Table(handle_doc):
    def affect_document(self,document,item):
        document.add_paragraph()
        self._load_table('tables/'+item.NAME+'.csv',document)
        r = re.compile('\d+')
        cap = caption(item.TEXT,r.search(item.ID).group())
        cap.addCaptionT(document)
        document.add_paragraph()#adds a default paragraph at the end

    def _load_table(self,filename,document):
        data = []
        with open(filename, 'rU') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                data.append(row)
        if data.__len__() > 0:
            t = document.add_table(rows= data.__len__(), cols= data[0].__len__()-1,style ='TableGrid') #adds a new table
            for i,row in enumerate(data):
                cells = t.rows[i].cells
                for j in range(1,row.__len__()):
                    cell = cells[j-1]#because we always skip the first column from data
                    text = row[j].replace('\xe2\x80\x99','\'')#in case of apostrophe
                    cell.text = unicode(text,errors = 'replace').replace(u"\ufffd", " ")
                    cell.paragraphs[0]._p.alignment = WD_ALIGN_PARAGRAPH.LEFT

class AllocT(handle_doc):
    def affect_document(self,document,item):
        filename = 'derived/'+item.NAME+'.csv'
        self._load_table_conc(filename,document)

    def _load_table_conc(self,filename,document):
        #import data from excel
        ref = ''
        with open(filename, 'rU') as csvfile:
            reader = csv.DictReader(csvfile)
            for row  in reader:
                ref += row['Der From'] + ', '
        ref = ref[:ref.__len__()-2]
        table = document.add_table(rows= 1, cols= 1,style ='TableGrid') #adds a new table with a single row
        table.rows[0].cells[0].text = unicode(ref,errors = 'replace').replace(u"\ufffd", " ")
        document.add_paragraph()

class par(handle_doc):
    def affect_document(self,document,item):
        paragraph = document.add_paragraph(item.TEXT)
        if item.IS_ASSUMPTION:
            document.add_paragraph()#add empty space before assumption
            paragraph = document.add_paragraph(item.ASSUMPTION)
        paragraph._p.alignment =  WD_ALIGN_PARAGRAPH.LEFT
        document.add_paragraph()#adds a default paragraph at the end

class Image(handle_doc):
    def affect_document(self,document,item):

        paragraph = document.add_paragraph()
        run = paragraph.add_run()
        r = re.compile('[1-9]+')#actual number except 0
        size = 6 #default size
        sizeword = item.IMAGE_SIZE
        if r.match(sizeword): #if an actual number was inserted as size except 0
            size = int(sizeword)
        run.add_picture('img/'+item.NAME+'.jpg',width = Inches(size))
        paragraph._p.alignment =  WD_ALIGN_PARAGRAPH.CENTER
        r = re.compile('\d+')
        cap = caption(item.TEXT,r.search(item.ID).group())
        cap.addCaptionF(document)
        document.add_paragraph()#adds a default paragraph at the end

class Req_tableform(handle_doc):

    def loadtable(self,document):
        from docx.enum.table import WD_TABLE_ALIGNMENT
        t = document.add_table(rows= 1, cols= 3,style ='TableGrid') #adds a new table
        t.alignment = WD_TABLE_ALIGNMENT.LEFT
        row = t.rows[0].cells
        row[0].text = 'ID'
        row[1].text = 'Text'
        row[2].text = 'Traceability'
        return t

    def affect_table(self,t,item):
        row = t.add_row().cells
        row[0].text = item.ID
        if not(item.IS_NAME_EMPTY):
            row[1].paragraphs[0].add_run(item.NAME,'bold')
        row[1].add_paragraph(item.TEXT)
        row[1].paragraphs[1]._p.alignment = WD_ALIGN_PARAGRAPH.LEFT
        if item.IS_ASSUMPTION:
            row[1].add_paragraph(item.ASSUMPTION)
            row[1].paragraphs[2]._p.alignment = WD_ALIGN_PARAGRAPH.LEFT
        row[2].text = item.SOURCE